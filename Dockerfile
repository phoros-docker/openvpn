FROM alpine:latest

ENV S6_OVERLAY_VERSION v1.21.7.0

# Terminate all s6 childs when init stage fails
ENV S6_BEHAVIOUR_IF_STAGE2_FAILS 2

# Install OpenVPN and EasyPKI, do some more work
RUN apk add --update --no-progress --no-cache bash dnsmasq easy-rsa openvpn tar \
    && wget https://github.com/just-containers/s6-overlay/releases/download/${S6_OVERLAY_VERSION}/s6-overlay-amd64.tar.gz \
    && tar -C / -xzvf s6-overlay-amd64.tar.gz \
    && rm s6-overlay-amd64.tar.gz \
    && rm -rf /var/cache/apk/* \
    && mkdir -p /var/openvpn/

# Copy OpenVPN configuration
COPY ./config/ /

# Copy s6-overlay files
COPY ./assets/s6-overlay/ /etc/

# Fix permissions
RUN chmod 755 /etc/ \
    && chmod -R a=r,u+w,a+X /etc/openvpn/

EXPOSE 1194
VOLUME ["/var/openvpn/"]
CMD [ "/init"]
