# Dockerized OpenVPN
This image provides a small OpenVPN image to connect to internal services in Docker networks.

# Features
* High security settings by default
* Resolves internal Docker hosts
* It provides OpenVPN (whoa!)

# Environment variables
| Variable | Type | Purpose |
| -------  | ---- | ------- |
| CREATE_CLIENT_CERTIFICATE | Boolean [false / true] | Enabling this flag will generate a default client certificate with its corresponding key. When you disable this flag you have to generate certificates by yourself. |

# Directories
This image has a special directory structure where files are located.

### Pre-defined
These directories are used by the image and should not be changed.

* /etc/openvpn/ - All server related files are located here

### Persistent
Use a persistent volume to make sure these files will survive a container removal.

* /var/openvpn/ - All generated certificates and keys by easy-rsa

# How to use
Creation of a container is pretty straightforward. Just follow this 4 easy steps:

1. Create a persistent volume: `docker volume create openvpn`
2. Create a container: `docker create --name openvpn --hostname openvpn.example.org --env CREATE_CLIENT_CERTIFICATE=true --mount type=volume,source=openvpn,destination=/var/openvpn/ --publish 1194:1194/udp --cap-add=NET_ADMIN registry.gitlab.com/phoros-docker/openvpn:latest`
3. Connect the container to a network: `docker network connect database openvpn`
4. Start the container: `docker run -d openvpn`

In order to connect to the server make sure you copy all needed files to your client. The image will create an archive `openvpn.tar.gz` in the containers `/tmp` directory with `ca.crt` and `ta.key`. When you enable the creation of client certificates it will also put `client.crt` and `client.key` into the archive. To copy this archive from the container use `docker cp openvpn:/tmp/openvpn.tar.gz .` which will put it into your current directory. Just transfer this archive to your client and unpack the files.

You will also need the OpenVPN client config file which you can find [here](client.ovpn). Take a look into it and change the options to fit your needs.

### Client certificate creation
When you disable the `CREATE_CLIENT_CERTIFICATE` flag you can create your own certificates inside the container.

```
cd /usr/share/easy-rsa/ # Change to the easy-rsa directory
./easyrsa build-client-full client [nopass] # Generate the client certificate and key (use nopass when you don't want to set a password)
```

# Security advice
Generating a client certificate on the server side is considered bad practice. You may want to copy all files of the container from `/var/openvpn/` before you generate your own client certificates using easy-rsa.
